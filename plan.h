#include <string>

using namespace std;

class Plan{
  private: 
  string __name;
  int __id;
  double __rate;
  string __period;

  public:
  Plan();
  Plan(string name, int id, double rate, string period);
  
  string getName();
  string getPeriod();

  int getID();

  double getRate();
};