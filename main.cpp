// Стандартные библиотеки STL
#include <iostream>
#include <vector>
#include <fstream>

// Файлы классов
#include "user.h"
#include "plan.h"
#include "usage.h"

using namespace std;

// Регионы просто позволяют отделять блоки кода, регион можно свернуть в иде чтоб нихера не мешало
#pragma region helpers
vector<string> split (string str, char seperator)  
{  
    int i = 0;  // итератор
    int startIndex = 0, endIndex = 0; // иднексы для определения размеров подстроки
    vector<string> res = vector<string>(); // Результирующий массив
    while (i <= str.length())  // Идём до конца строки 
    {  
        if (str[i] == seperator || i == str.length()) // Если дошли до сепаратора или конца 
        {  
            endIndex = i;  // приравниваем иднекс конца к текущему
            string subStr = "";  // обнуляем подстроку
            subStr.append(str, startIndex, endIndex - startIndex);  // добавляем к подстроке промежуток оригинальной строки между конечным и началным индексом
            res.push_back(subStr);  // подстрока записывается в результат
            startIndex = endIndex + 1;  // смещаем индекс начала подстроки
        }  
        i++;  
        }   
  return res;  
} 
#pragma endregion helpers


#pragma region input_functions
// Чтение списка пользователей и запись в массив
void getUsers(vector<User> *users){
  ifstream usersReadFile("users.txt"); // Поток на ввод из файла
  string in; // Входная строка

  while(getline(usersReadFile, in)){ // Пока не перестанет читать строки с файла - работать
    vector<string> splitIn = split(in, ','); // Разбиение строки на подстроки и запись в вектор
    // При работе с указателями используются стрелки, а не точки
    users->push_back(User(splitIn[0], splitIn[1], splitIn[2], splitIn[3], stoi(splitIn[4]), stoi(splitIn[5]))); // Пихаем в вектор нового пользователя
  }

  usersReadFile.close(); // Закрываем поток на вход
}

void getPlans(vector<Plan> *plans){
  ifstream plansReadFile("plans.txt"); // Поток на ввод из файла
  string in; // Входная строка

  while(getline(plansReadFile, in)){ // Пока не перестанет читать строки с файла - работать
    vector<string> splitIn = split(in, ','); // Разбиение строки на подстроки и запись в вектор
    // При работе с указателями используются стрелки, а не точки
    plans->push_back(Plan(splitIn[0], stoi(splitIn[1]), stod(splitIn[2]), splitIn[3])); // Пихаем в вектор новый тариф
  }

  plansReadFile.close(); // Закрываем поток на вход
}

void getUsage(vector<Usage> *usageLogs){
  ifstream usageReadFile("usage.txt"); // Поток на ввод из файла
  string in; // Входная строка

  while(getline(usageReadFile, in)){ // Пока не перестанет читать строки с файла - работать
    vector<string> splitIn = split(in, ','); // Разбиение строки на подстроки и запись в вектор
    // При работе с указателями используются стрелки, а не точки
    int time = splitIn[3] == "#" ? 0 : stoi(splitIn[3]); 
    usageLogs->push_back(Usage(splitIn[0], stoi(splitIn[1]), splitIn[2], time)); // Пихаем в вектор новую запись об использовании тарифв
  }

  usageReadFile.close(); // Закрываем поток на вход
}
#pragma endregion input_fucntions


int main()
{
  vector<User> users = vector<User>();
  getUsers(&users); // Передаём указатель, чтоб изменить сам вектор, а не его копию
  vector<Plan> plans = vector<Plan>();
  getPlans(&plans);
  vector<Usage> usageLogs = vector<Usage>();
  getUsage(&usageLogs);


  ofstream outFile("Report.txt");

  vector<string> resUsernames = vector<string>(); // массив результатов
  for(int i = 0; i < usageLogs.size(); i++){
    vector<string> dateSplit = split(usageLogs[i].getDate(), ' '); // разбиение даты и времени на разные строки
    vector<string> timeSplit = split(dateSplit[1], ':'); // разбиение времени на часы, минуты и секунды

    int startseconds = stoi(timeSplit[0]) * 3600 + stoi(timeSplit[1]) * 60 + stoi(timeSplit[2]); // время начала в секундах
    int endseconds = startseconds + usageLogs[i].getTime(); // время конца в секундах
    if (
      (startseconds >= 39600 && startseconds <= 61200)  // 39600 = 11*3600, 61200 = 17*3600
      || (endseconds >= 39600 && startseconds <= 61200) ){ // Если начали с 11 до 17 или если закончии после 11 и начали до 17

      string customerName; 
      // поиск имени клиента по номеру  
      for (int j = 0; j < users.size(); j++){
        if (usageLogs[i].getCustomerPhone() == users[j].getPhone()){
          customerName = users[i].getName();
        }
      }
      cout << customerName << '\n';
      resUsernames.push_back(customerName);
    }
  }

  // Вывод
  if(resUsernames.size()) {
    for(int i = 0; i < resUsernames.size(); i++){
      outFile << resUsernames[i] << '\n';
    }
  } else {
    cout << "No Data";
  }


}