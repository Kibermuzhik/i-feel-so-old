#include <string>

using namespace std;

class Usage {
  private: 
  string __customerPhone;
  int __planId;
  string __date;
  int __time;

  public:
  Usage();
  Usage(string phone, int id, string date, int time);

  string getCustomerPhone();
  string getDate();

  int getPlanID();
  int getTime();
};