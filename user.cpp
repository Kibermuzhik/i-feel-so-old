#include "user.h"
#include <iostream>

// Конструктор по умолчанию реализованный через список инициализации
User::User() :
__name("N/a"),
__phone("N/a"),
__startDate("N/a"),
__endDate("N/a"),
__debt(0),
__credit(0)
{};

// Контруктор с параметрами
User::User(string name, string phone, string start, string end, int debt, int credit){
  __name = name;
  __phone = phone;
  __startDate = start;
  __endDate = end;
  __debt = debt;
  __credit = credit;
}

string User::getName(){
  return __name;
}

string User::getPhone(){
  return __phone;
}

string User::getStartDate(){
  return __startDate;
}

string User::getEndDate(){
  return __endDate;
}

int User::getDebt(){
  return __debt;
}

int User::getCredit(){
  return __credit;
}

