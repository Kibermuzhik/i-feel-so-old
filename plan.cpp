#include "plan.h"

Plan::Plan() : 
__name("N/A"),
__id(-1),
__rate(0.0),
__period("#")
{};

Plan::Plan(string name, int id, double rate, string period):
__name(name),
__id(id),
__rate(rate),
__period(period)
{};

string Plan::getName(){
  return __name;
}

string Plan::getPeriod(){
  return __period;
}

int Plan::getID(){
  return __id;
}

double Plan::getRate(){
  return __rate;
}