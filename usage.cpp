#include "usage.h"

using namespace std;

Usage::Usage():
__customerPhone("N/A"),
__planId(-1),
__date("N/A"),
__time(0)
{}

Usage::Usage(string phone, int id, string date, int time):
__customerPhone(phone),
__planId(id),
__date(date),
__time(time)
{}

string Usage::getCustomerPhone(){
  return __customerPhone;
}

string Usage::getDate(){
  return __date;
}

int Usage::getPlanID(){
  return __planId;
}

int Usage::getTime(){
  return __time;
}