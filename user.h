#include <string>

using namespace std;

class User {
  private: 
   string __name; // ФИО
   string __phone; // Телефон
   string __startDate; // Дата заключения договора
   string __endDate; // Дата окончания договора
   int __debt; // Размер задолженности
   int __credit; // Допустимый кредит

   // Паблик, потому что осталным вещам это нужно
  public:
  User();
  User(string name, string phone, string start, string end, int bedt, int credit);

  // Методы для получения значений полей объектаб позволяет работать с ними без риска что-то сломать
  string getName();
  string getPhone();
  string getStartDate();
  string getEndDate();
  int getDebt();
  int getCredit();
};